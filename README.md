# README #

### What is this repository for? ###

* Created by Hammad Hussain Qureshi
* Date: 12 November 2017
* Insydo Assessment Test

### How do I get set up? ###

* Fetch this Django applcication Repository
git clone project-git-link
* Dependencies
pip install -r requirements.txt
* Elastic Search configuration
Make sure elasticsearch instance is accessible at http://localhost:9200/ 
* How to run tests
For Test links are given in the provided answer sheet. Or use index page containing links for various test questions
* Run Django Commands
python manage.py migrate
python manage.py runserver settings=insydo_test.settings 
