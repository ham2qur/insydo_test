from django.conf.urls import url, include
from question4.views import shuffle_list

urlpatterns = [
    
    url(r'^question/4/', shuffle_list, name="question4"),
]