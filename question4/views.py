import random
from django.shortcuts import render
from django.http.response import HttpResponse


def shuffle_list(request):
    
    given_list = [-1122, 2321, -9023, 2711, 8112, -912, 2711, 9832]
    
    shuffled_list = _shuffle_list_values(given_list)
    
    result = 'input = ' +\
        ', '.join([str(x) for x in given_list]) +\
        '\n output = ' +\
        ', '.join([str(x) for x in shuffled_list])
    
    return HttpResponse(result)

def _shuffle_list_values(ids_list):
      
    random_movements = {n_id: random.randrange(-1, 2, 1) for n_id in ids_list}
    left = -1
    right = 1
      
    shuffled_list = ids_list[:]
    for index, n_id in enumerate(ids_list):
        position = index + random_movements[n_id]
        if not( index == 0 and random_movements[n_id] == left ) and\
            not( index == len(ids_list) - 1 and random_movements[n_id] == right ) and\
            index == shuffled_list.index(n_id) and\
            shuffled_list[position] == ids_list[position]: 
                
                shuffled_list.insert(position, shuffled_list.pop(shuffled_list.index(n_id)))   
    
    return shuffled_list