import json
import os

from django.conf import settings
from django.contrib import messages
from django.http.response import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, redirect
from elasticsearch import Elasticsearch


ELASTICSEARCH_HOST = getattr(settings, 'ELASTICSEARCH_HOST')
ELASTICSEARCH_PORT = getattr(settings, 'ELASTICSEARCH_PORT')
BASE_DIR = getattr(settings, 'BASE_DIR')

es = Elasticsearch([{'host': ELASTICSEARCH_HOST, 'port': ELASTICSEARCH_PORT}])

def load_json_data():
    json_data = open(BASE_DIR + '/static/data.txt')
    data = json.load(json_data)
    business = data.get('business')
    i = 0
    while i != len(business):
        es.index(index='business', doc_type='business', id=i, body=business[i])
        i = i + 1
    
    json_data.close()

if not es.indices.exists(index='business'):
    load_json_data()
    
def search(request):
    
    if not request.method == 'GET':
        return HttpResponseBadRequest('Only GET allowed')
    
    kw = request.GET.get('q')
    query = {"query": {"match": {"name": kw}}}
    search_res = es.search(index='business', doc_type='business', body=query)
    search_hits = search_res.get('hits')
    data = search_hits.get('hits')
    messages.add_message(request, messages.SUCCESS, data)
    return redirect('question3_page')

def question3(request):
    context = {}
    return render(request, 'question3/search.html', context)    