from django.test import TestCase


class Question3TestCase(TestCase):

    def test_question3_page(self):
        resp = self.client.get('/question/3')
        self.assertEqual(resp.status_code, 200)
        
    def test_elasticsearch_connection(self):
        resp = self.client.get('http://localhost:9200/')
        self.assertEqual(resp.status_code, 200)
        
