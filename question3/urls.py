from django.conf.urls import url, include
from question3.views import question3, search

urlpatterns = [
    
    url(r'^question/3', question3, name="question3_page"),
    url(r'^search', search, name="question3_search"),
]