from django.conf.urls import url
from django.views.generic.base import TemplateView
from django.shortcuts import render


urlpatterns = [
    
    url(r'^', lambda r: render(r, 'question1/index.html', {}), name='index'),
]