# -*- coding: utf-8 -*-
'''
Question 1: part 2 
Register that model in Django Admin
'''

from __future__ import unicode_literals

from django.contrib import admin

from question1.models import TestModel


@admin.register(TestModel)
class SampleModelAdmin(admin.ModelAdmin):

    def has_add_permission(self, request):
        # check if generally has add permission
        retVal = super(SampleModelAdmin, self).has_add_permission(request)
        # set add permission to False, if object already exists
        blocked = TestModel.get_instance(block=True)
        if retVal and blocked is None:
            retVal = False
        return False
    

