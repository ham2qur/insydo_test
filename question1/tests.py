from django.test import TestCase
from question1.models import TestModel


class Question1_ModelTestCase(TestCase):
    
    fixtures = ['model_testdata.json']    

    def setUp(self):
        super(Question1_ModelTestCase, self).setUp()
        self.data1 = TestModel.objects.get(pk=1)
        self.data2 = TestModel.objects.get(pk=2)

    def test_data1_values(self):
        self.assertEqual(self.data1.attribute1, 'a')
        self.assertEqual(self.data1.attribute2, 'b')
        self.assertEqual(self.data1.attribute3, 'c')
        self.assertEqual(self.data1.attribute4, 'd')
        
    def test_data2_values(self):
        self.assertEqual(self.data2.attribute1, 'e')
        self.assertEqual(self.data2.attribute2, 'f')
        self.assertEqual(self.data2.attribute3, 'g')
        self.assertEqual(self.data2.attribute4, 'h')        

class Question1_TestUrls(TestCase):
    
    def test_index(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)
    
    def test_admin_url(self):
        resp = self.client.get('/admin')
        self.assertEqual(resp.status_code, 200)
