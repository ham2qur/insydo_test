# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.gis.db.models import fields
from django.db import models

testmodelblocked = False

class TestModel(models.Model):
    
    attribute1 = models.CharField('Attribute 1', null=True, max_length=40)
    attribute2 = models.CharField('Attribute 2', null=True, max_length=40)
    attribute3 = models.CharField('Attribute 3', null=True, max_length=40)
    attribute4 = models.CharField('Attribute 4', null=True, max_length=40)

    @staticmethod
    def get_instance(block=False):
        global testmodelblocked
        if not testmodelblocked:
            testmodelblocked = block
            return TestModel()
        
        return None
